﻿import { Roles } from './roles';

export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    token: string;
    role: string;
    loginTime?: Date;
    logoutTime?: Date;
    ip?: string;
}