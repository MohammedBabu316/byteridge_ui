import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { UserService, AuthenticationService } from '@/_services';
import { Router } from '@angular/router';

@Component({
  templateUrl: './auditor.component.html'})
export class AuditorComponent implements OnInit {

  users = [];
  dtOptions: DataTables.Settings ={
  };
  isFetched : Promise<boolean>;

  constructor(
      private router: Router,
      private userService: UserService
  ) {
  }


  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      lengthMenu: [5,10,20,30],
      processing: true
    };
    this.userService.audit().subscribe((data) => {
      this.users = [...data]
      this.users.forEach((user)=>{
        user.role = user.role ? user.role.toLowerCase() : "";
        user.loginTime = new Date(user.loginTime);
        user.logoutTime = new Date(user.logoutTime)
      })
      this.isFetched = Promise.resolve(true);
    }, (err) => {
        console.log('auditor data fetch error', err);
        this.router.navigate(['/']);
      }
    );    
}

}
