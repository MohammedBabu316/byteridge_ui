﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './_services';
import { User } from './_models';

import { Roles } from './_models/roles';


import './_content/app.less';

@Component({ selector: 'app', templateUrl: 'app.component.html' })
export class AppComponent{
    currentUser: User;
    isAuditor: Boolean = false;
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        this.authenticationService.currentUser.subscribe(x =>{
            this.currentUser = x
            if(this.currentUser && this.currentUser.role && (this.currentUser.role.toLocaleLowerCase() === Roles.Auditor.toLocaleLowerCase())){
                this.isAuditor = true;
            }
        });
    }

    logout() {
        this.authenticationService.logout(this.currentUser.username);
    }
}