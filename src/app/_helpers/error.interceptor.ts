import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from '@/_services';
import { User } from '../_models';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    currentUser: User;
    constructor(private authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.currentUser = this.authenticationService.currentUserValue;
                this.authenticationService.logout(this.currentUser.username);
            }
            
            const error = err.error.message || err.statusText;
            return throwError(error);
        }))
    }
}