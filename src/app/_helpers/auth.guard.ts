﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthenticationService } from '@/_services';
import { Roles } from '@/_models/roles';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;
        // console.log('user in guard',currentUser,this.router.url)
        if (currentUser) {
            // check if route is restricted by role
            if(this.router.url === '/') return true;
            else if (this.router.url === '/audit' && currentUser.role && currentUser.role.toLocaleLowerCase() !== Roles.Auditor.toLocaleLowerCase()) {
                // role not authorised so redirect to home page
                console.log('auditor roles not matched')
                return false;
            }
            else return true;
        }
        console.log('here at login navigate')
        this.router.navigate(['/login']);
        return false;
    }
}
